// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import com.intellij.ide.projectView.ProjectView;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VfsUtil;
import org.apache.commons.lang3.SystemUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Helpers {

    public static boolean validateQtCmakeFile(String path) {
        return Paths.get(path).getFileName().toString().matches("(^qt-cmake(\\.bat)?$)")
                && new File(path).exists();
    }

    public static String getCMakePathFromQtRoot(@NotNull String qtRootPath)
            throws SecurityException {
        String cmakePath = "";

        if (SystemUtils.IS_OS_WINDOWS) {
            cmakePath = qtRootPath + "\\Tools\\CMake_64\\bin\\cmake.exe";
        } else if (SystemUtils.IS_OS_MAC_OSX) {
            cmakePath = qtRootPath + "/Tools/CMake/CMake.app/Contents/bin/cmake";
        } else if (SystemUtils.IS_OS_LINUX) {
            cmakePath = qtRootPath + "/Tools/CMake/bin/cmake";
        }

        if (!new File(cmakePath).exists()) {
            cmakePath = "";
        }

        return cmakePath;
    }

    public static String getNinjaPathFromQtRoot(@NotNull String qtRootPath)
            throws InvalidPathException, SecurityException {
        String ninjaFileName = "ninja";
        if (SystemUtils.IS_OS_WINDOWS) {
            ninjaFileName += ".exe";
        }

        String ninjaPath = Paths.get(qtRootPath, "Tools", "Ninja", ninjaFileName).toString();

        if (new File(ninjaPath).exists()) {
            return ninjaPath;
        }

        return "";
    }

    public static boolean validateQtInstallRootPath(String path) {
        if (!Files.exists(Path.of(path)))
            return false;

        try {
            return !getCMakePathFromQtRoot(path).isBlank() && !getNinjaPathFromQtRoot(path).isBlank();
        } catch (InvalidPathException | SecurityException ex) {
            return false;
        }
    }

    public static void refreshProjectView(Project project) {
        ProjectView.getInstance(project).refresh();

        QtSettingStateProject settings = QtSettingStateProject.getInstance(project);
        if (!settings.getQtProjectPath().isBlank()) {
            VfsUtil.markDirtyAndRefresh(
                    true, // async
                    true, // recursive
                    true, // reloadChildren
                    VfsUtil.findFileByIoFile(new File(settings.getQtProjectPath()),
                            true) // files
            );
        }
    }

    /**
     * Get absolute path from a relative path with respect to a base file
     * @param baseFilePath The base file from which relative path is calculated
     * @param relativePath The relative path
     * @return Absolute path of the relative path
     */
    public static String getAbsolutePath(String baseFilePath, String relativePath) throws IOException {
        File relativeFile = new File(relativePath);
        if (relativeFile.isAbsolute()) {
            return relativePath;
        }

        File baseFile = new File(baseFilePath);
        File parentFolder = new File(baseFile.getParent());
        File absoluteFile = new File(parentFolder, relativePath);
        return absoluteFile.getCanonicalPath();
    }

    public static boolean pathContainsAndroidABI(String path) {
        File filePath = new File(path);
        if (filePath.exists()) {
            for (File file : filePath.listFiles()) {
                if (file.getName().matches("android_(x86_64|arm64_v8a|armv7|x86)"))
                    return true;
            }
        }
        return false;
    }

    public static String escapeSingleBackSlash(String path) {
        return Paths.get(path).toString().replace("\\", "\\\\");
    }

    public static String formatBinaryName(String binary) {
        if (SystemUtils.IS_OS_WINDOWS)
            binary += ".exe";

        return binary;
    }
}

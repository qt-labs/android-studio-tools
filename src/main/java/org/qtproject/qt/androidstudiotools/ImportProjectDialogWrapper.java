// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;

import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.ui.ComponentValidator;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.TextBrowseFolderListener;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.DocumentAdapter;
import com.intellij.util.ui.FormBuilder;

import org.jetbrains.annotations.Nullable;

class ImportProjectDialogWrapper extends DialogWrapper {
    private JLabel m_errorLabel;

    private final TextFieldWithBrowseButton m_qtPathLocationBtn = new TextFieldWithBrowseButton();
    private final TextFieldWithBrowseButton m_newQtProjectLocationBtn = new TextFieldWithBrowseButton();

    protected ImportProjectDialogWrapper() {
        super(true);

        setTitle("Import Qt Project");
        init();

        FileChooserDescriptor allowFoldersFCD = new FileChooserDescriptor(
                false, true, false, false, false, false
        );

        m_qtPathLocationBtn.addBrowseFolderListener(new TextBrowseFolderListener(allowFoldersFCD));
        m_newQtProjectLocationBtn.addBrowseFolderListener(new TextBrowseFolderListener(allowFoldersFCD));

        ComponentValidator qtPathValidator = new ComponentValidator(getDisposable()).withValidator(
                () -> validateQtPath()).installOn(m_qtPathLocationBtn);

        m_qtPathLocationBtn.getTextField().getDocument().addDocumentListener(new DocumentAdapter() {
            @Override
            public void textChanged(DocumentEvent e) {
                qtPathValidator.revalidate();
            }
        });

        ComponentValidator projectPathValidator = new ComponentValidator(
                getDisposable()).withValidator(() -> validateProjectPath())
                .installOn(m_newQtProjectLocationBtn);

        m_newQtProjectLocationBtn.getTextField().getDocument()
                .addDocumentListener(new DocumentAdapter() {
                    @Override
                    public void textChanged(DocumentEvent e) {
                        projectPathValidator.revalidate();
                    }
                });

        startTrackingValidation();

        Dimension minSize = getWindow().getMinimumSize();
        minSize.width = 600;
        getWindow().setMinimumSize(minSize);
    }

    @Nullable
    @Override
    protected ValidationInfo doValidate() {
        if (validateQtPath() != null || validateProjectPath() != null) {
            return new ValidationInfo("");
        }
        return null;
    }

    private ValidationInfo validateQtPath() {
        if (!Helpers.pathContainsAndroidABI(m_qtPathLocationBtn.getText())) {
            return new ValidationInfo("No valid Qt for Android kits found in the provided Qt path.",
                    m_qtPathLocationBtn.getTextField());
        }
        return null;
    }

    private ValidationInfo validateProjectPath() {
        String newProjectLocation = Helpers.escapeSingleBackSlash(m_newQtProjectLocationBtn.getText());
        boolean checkCmake = new File(newProjectLocation, "CMakeLists.txt").exists();
        if (newProjectLocation.isBlank() || !checkCmake) {
            return new ValidationInfo("No CMakeLists.txt found under the provided Qt project path.",
                    m_newQtProjectLocationBtn.getTextField());
        }
        return null;
    }

    @Override
    protected void doOKAction() {
        m_errorLabel.setVisible(false);
        String newProjectLocation = Helpers.escapeSingleBackSlash(m_newQtProjectLocationBtn.getText());
        String qtPathLocation = Helpers.escapeSingleBackSlash(m_qtPathLocationBtn.getText());

        // Close the current dialog before starting the new one
        dispose();

        CodeSnippetDialogWrapper dialog = new CodeSnippetDialogWrapper(newProjectLocation, qtPathLocation);
        dialog.show();

        super.doOKAction();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        m_errorLabel = new JLabel();
        m_errorLabel.setForeground(Color.RED);
        m_errorLabel.setVisible(false);

        return FormBuilder.createFormBuilder()
                .addLabeledComponent(
                        new JLabel("Qt Location: "),
                        m_qtPathLocationBtn,
                        1,
                        false
                ).addLabeledComponent(
                        new JLabel("Qt Project Location: "),
                        m_newQtProjectLocationBtn,
                        1,
                        false
                ).addComponent(m_errorLabel)
                .getPanel();
    }
}

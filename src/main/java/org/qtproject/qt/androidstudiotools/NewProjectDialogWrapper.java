// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;

import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.ui.ComponentValidator;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.TextBrowseFolderListener;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.components.JBTextField;
import com.intellij.util.ui.FormBuilder;
import org.jetbrains.annotations.Nullable;

// TODO: QTBUG-122062 Separate the new project dialog
class NewProjectDialogWrapper extends DialogWrapper {

    private final Project m_project;
    private String m_newProjectLocation;
    private String m_qtPath;
    private JLabel m_errorLabel;
    private final TextFieldWithBrowseButton m_qtPathLocationBtn = new TextFieldWithBrowseButton();
    private final JBTextField m_newQtProjectNameField = new JBTextField();
    private final TextFieldWithBrowseButton m_newQtProjectLocationBtn = new TextFieldWithBrowseButton();
    private final ComboBox<String> m_minimumQtVersionComboBox = new ComboBox<>();
    private final static String[] m_minQtVersions = new String[]{"6.8"};

    protected NewProjectDialogWrapper(@Nullable Project project) {
        super(project, true);

        m_project = project;

        setTitle("Create New Qt Project");
        init();

        for (String s : m_minQtVersions) {
            m_minimumQtVersionComboBox.addItem(s);
        }

        FileChooserDescriptor allowFoldersFCD = new FileChooserDescriptor(
                false, true, false, false, false, false
        );

        m_qtPathLocationBtn.addBrowseFolderListener(new TextBrowseFolderListener(allowFoldersFCD));
        m_newQtProjectLocationBtn.addBrowseFolderListener(new TextBrowseFolderListener(allowFoldersFCD));

        ComponentValidator projectNameValidator = new ComponentValidator(
                getDisposable()).withValidator(() -> validateProjectName())
                .installOn(m_newQtProjectNameField);

        m_newQtProjectNameField.getDocument().addDocumentListener(new DocumentAdapter() {
            @Override
            public void textChanged(DocumentEvent e) {
                projectNameValidator.revalidate();
                NewProjectDialogWrapper.this.initValidation();
            }
        });

        ComponentValidator qtPathValidator = new ComponentValidator(getDisposable()).withValidator(
                () -> validateQtPath()).installOn(m_qtPathLocationBtn);

        m_qtPathLocationBtn.getTextField().getDocument().addDocumentListener(new DocumentAdapter() {
            @Override
            public void textChanged(DocumentEvent e) {
                qtPathValidator.revalidate();
            }
        });

        ComponentValidator projectPathValidator = new ComponentValidator(
                getDisposable()).withValidator(() -> validateProjectPath())
                .installOn(m_newQtProjectLocationBtn);

        m_newQtProjectLocationBtn.getTextField().getDocument()
                .addDocumentListener(new DocumentAdapter() {
                    @Override
                    public void textChanged(DocumentEvent e) {
                        projectPathValidator.revalidate();
                    }
                });

        startTrackingValidation();

        Dimension minSize = getWindow().getMinimumSize();
        minSize.width = 600;
        getWindow().setMinimumSize(minSize);
    }

    @Nullable
    @Override
    protected ValidationInfo doValidate() {
        if (validateProjectName() != null || validateQtPath() != null ||
                validateProjectPath() != null) {
            return new ValidationInfo("");
        }
        return null;
    }

    private ValidationInfo validateProjectName() {
        String text = m_newQtProjectNameField.getText();
        // Project name will be used as the QML module name
        // Module names with dots are not supported yet [QTBUG-125891]
        if (!text.matches("^[a-zA-Z][a-zA-Z0-9_-]*")) {
            return new ValidationInfo(
                    "Start with a letter, use only letters, digits, dashes, or underscores.",
                    m_newQtProjectNameField);
        }
        return null;
    }

    private ValidationInfo validateQtPath() {
        if (!Helpers.pathContainsAndroidABI(m_qtPathLocationBtn.getText())) {
            return new ValidationInfo("No valid Qt for Android kits found in the provided Qt path.",
                    m_qtPathLocationBtn.getTextField());
        }
        return null;
    }

    private ValidationInfo validateProjectPath() {
        if (m_newQtProjectLocationBtn.getTextField().getText().isBlank())
            return new ValidationInfo("Project path can not be empty.",
                    m_newQtProjectLocationBtn.getTextField());

        return null;
    }

    private void showError(String text) {
        m_errorLabel.setText(text);
        m_errorLabel.setVisible(true);
    }

    @Override
    protected void doOKAction() {
        m_errorLabel.setVisible(false);
        m_qtPath = Helpers.escapeSingleBackSlash(m_qtPathLocationBtn.getText());
        String newProjectName = m_newQtProjectNameField.getText();
        m_newProjectLocation = Helpers.escapeSingleBackSlash(String.format("%s/%s",
                m_newQtProjectLocationBtn.getText(), newProjectName));

        if (!new File(m_newProjectLocation).mkdirs()) {
            showError("Cannot create directory at the given location");
            return;
        }

        QtSettingStateProject settingsProject = QtSettingStateProject.getInstance(m_project);
        settingsProject.setQtMinimumVersion( (String) m_minimumQtVersionComboBox.getSelectedItem());

        super.doOKAction();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        m_errorLabel = new JLabel();
        m_errorLabel.setForeground(Color.RED);
        m_errorLabel.setVisible(false);

        return FormBuilder.createFormBuilder()
                .addLabeledComponent(
                        new JLabel("Qt Project Name: "),
                        m_newQtProjectNameField,
                        1,
                        false
                ).addLabeledComponent(
                        new JLabel("Project Location: "),
                        m_newQtProjectLocationBtn,
                        1,
                        false
                ).addLabeledComponent(
                        new JLabel("Qt Location: "),
                        m_qtPathLocationBtn,
                        1,
                        false
                ).addLabeledComponent(
                        new JLabel("Minimum Qt Version: "),
                        m_minimumQtVersionComboBox,
                        1,
                        false
                ).addComponent(m_errorLabel)
                .getPanel();
    }

    public static String getMinimumQtVersionExample(){
        return m_minQtVersions[0];
    }

    public String getNewProjectLocation() {
        return m_newProjectLocation;
    }

    public String getQtPath() {
        return m_qtPath;
    }

    public String getMinimumQtVersion() {
        return (String) m_minimumQtVersionComboBox.getSelectedItem();
    }
}

// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import com.intellij.openapi.actionSystem.ActionUpdateThread;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

class NewProjectAction extends AnAction {

    @Override
    public @NotNull ActionUpdateThread getActionUpdateThread() {
        return ActionUpdateThread.BGT;
    }

    @Override
    public void update(@NotNull AnActionEvent e) {
        super.update(e);

        boolean androidProjectReady = e.getProject() != null && e.getProject().isOpen();

        e.getPresentation().setVisible(androidProjectReady);
        e.getPresentation().setEnabled(androidProjectReady);
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        if (e.getProject() == null) {
            return;
        }

        NewProjectDialogWrapper wrapper = new NewProjectDialogWrapper(e.getProject());
        if (wrapper.showAndGet()) {
            // User clicks OK
            ProgressManager.getInstance().run(new Task.Backgroundable(e.getProject(), "Creating new Project") {
                @Override
                public void onSuccess() {
                    CodeSnippetDialogWrapper dialog = new CodeSnippetDialogWrapper(wrapper.getNewProjectLocation(),
                            wrapper.getQtPath());
                    dialog.show();
                    super.onSuccess();
                }

                @Override
                public void run(@NotNull ProgressIndicator indicator) {
                    try {

                        String projectLocation = wrapper.getNewProjectLocation();
                        String projectName = Paths.get(projectLocation).getFileName().toString();

                        // TODO QTBUG-122061: Improve CMakeLists.txt template usage
                        InputStream in = getClass().getResourceAsStream(
                                "/templates/qtquickapplication/CMakeLists.txt");
                        if (in != null) {
                            String content = IOUtils.toString(in, StandardCharsets.UTF_8);
                            content = content.replaceAll("%TARGET_NAME%", projectName + "App");
                            content = content.replaceAll("%QML_MODULE_NAME%", projectName);
                            content = content.replaceAll("%QT_MIN_VERSION%", wrapper.getMinimumQtVersion());
                            IOUtils.write(content, new FileOutputStream(
                                            Paths.get(projectLocation, "CMakeLists.txt").toFile()),
                                    StandardCharsets.UTF_8);
                        }

                        // main.cpp
                        in = getClass().getResourceAsStream(
                                "/templates/qtquickapplication/main.cpp");
                        if (in != null) {
                            IOUtils.write(
                                    IOUtils.toString(in, StandardCharsets.UTF_8),
                                    new FileOutputStream(Paths.get(projectLocation, "main.cpp").toFile()),
                                    StandardCharsets.UTF_8);
                        }

                        // Main.qml
                        in = getClass().getResourceAsStream(
                                "/templates/qtquickapplication/Main.qml");
                        if (in != null) {
                            IOUtils.write(
                                    IOUtils.toString(in, StandardCharsets.UTF_8),
                                    new FileOutputStream(Paths.get(projectLocation, "Main.qml").toFile()),
                                    StandardCharsets.UTF_8);
                        }

                        // gitignore
                        in = getClass().getResourceAsStream(
                                "/templates/qtquickapplication/gitignore");
                        if (in != null) {
                            IOUtils.write(
                                    IOUtils.toString(in, StandardCharsets.UTF_8),
                                    new FileOutputStream(Paths.get(projectLocation, ".gitignore").toFile()),
                                    StandardCharsets.UTF_8);
                        }

                        Helpers.refreshProjectView(e.getProject());

                    } catch (IOException | NullPointerException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        }
    }
}

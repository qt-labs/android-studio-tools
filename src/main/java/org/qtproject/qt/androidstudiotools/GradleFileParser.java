// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GradleFileParser {

    private static String removeBlockComments(String codeBlock) {
        return codeBlock.replaceAll("/\\*[^*]*\\*+([^/][^*]*\\*+)*/", "");
    }

    private static String getQtBuildBlockContents(File gradleFile) {
        try {
            String gradleContents = new String(Files.readAllBytes(gradleFile.toPath()));
            gradleContents = removeBlockComments(gradleContents);
            final Pattern qtBuildBlockPattern = Pattern.compile(
                    "^\\s*QtBuild\\s*\\{\\s*([^}]*)\\s*", Pattern.MULTILINE);
            final Matcher matcher = qtBuildBlockPattern.matcher(gradleContents);

            if (matcher.find())
                return matcher.group(1);

        } catch (IOException ignored) {
            System.out.println("Cannot read Gradle file " + gradleFile.getAbsolutePath());
        }

        return "";
    }

    private static String getQtBuildVariableValue(File gradleFile, String codeBlock, String key,
                                                  boolean isPath) {
        final Pattern attributesPattern = Pattern.compile(
                "^\\s*" + key + "[^\"']*[\"'](.*?)[\"'].?",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        final Matcher matcher = attributesPattern.matcher(codeBlock);

        if (matcher.find()) {
            if (!isPath)
                return matcher.group(1);

            try {
                return Helpers.getAbsolutePath(gradleFile.getAbsolutePath(), matcher.group(1));
            } catch (IOException ignored) {
                System.out.println("Error constructing a canonical path for " + matcher.group(1));
            }
        }

        return "";
    }

    public static void parseGradleFile(@NotNull Project project) {
        String projectBasePath = project.getBasePath();
        if (projectBasePath == null || projectBasePath.isBlank())
            return;

        Path projectDir = Paths.get(projectBasePath, "app");
        File gradleFile = null;
        File[] projectFiles = projectDir.toFile().listFiles();
        if (projectFiles != null) {
            for (File file : projectFiles) {
                if (file.getName().matches("build\\.gradle(\\.kts)?")) {
                    gradleFile = file;
                    break;
                }
            }
        }

        if (gradleFile == null)
            return;

        String qtBuildBlockContents = getQtBuildBlockContents(gradleFile);
        String projectPath = getQtBuildVariableValue(gradleFile, qtBuildBlockContents,
                "projectPath", true);
        String qtPath = getQtBuildVariableValue(gradleFile, qtBuildBlockContents,
                "qtPath", true);

        QtSettingStateProject projectSettings = QtSettingStateProject.getInstance(project);
        projectSettings.setQtProjectPath(projectPath);
        projectSettings.setQtPath(qtPath);

        Helpers.refreshProjectView(project);
    }
}

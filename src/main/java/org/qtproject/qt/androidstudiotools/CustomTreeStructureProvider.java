// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import com.intellij.ide.projectView.TreeStructureProvider;
import com.intellij.ide.projectView.ViewSettings;
import com.intellij.ide.util.treeView.AbstractTreeNode;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

class CustomTreeStructureProvider implements TreeStructureProvider {
    @Override
    public @NotNull Collection<AbstractTreeNode<?>> modify(
            @NotNull AbstractTreeNode<?> parent,
            @NotNull Collection<AbstractTreeNode<?>> children,
            ViewSettings settings
    ) {
        QtSettingStateProject qtSettings = QtSettingStateProject.getInstance(parent.getProject());
        ArrayList<AbstractTreeNode<?>> nodes = new ArrayList<>(children);
        if (parent.getName() != null && parent.getName().equals(parent.getProject().getName()) &&
                !parent.canNavigate() &&
                !qtSettings.getQtProjectPath().isBlank()) {
            if (!Files.exists(Paths.get(qtSettings.getQtProjectPath()))) {
                qtSettings.setQtProjectPath("");
            } else {
                VirtualFile dir = VirtualFileManager.getInstance().findFileByNioPath(
                        Paths.get(qtSettings.getQtProjectPath()));
                if (dir != null) {
                    nodes.add(new QtProjectNode(parent.getProject(), dir));
                }
            }
        }

        return nodes;
    }
}

// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.ui.Messages;
import org.jetbrains.annotations.Nullable;

import javax.swing.JComponent;

class QtSettingsConfigurable implements Configurable {

    private QtSettingPanel m_qtSettingPanel;

    @Override
    public String getDisplayName() {
        return "Qt";
    }

    @Override
    public @Nullable JComponent getPreferredFocusedComponent() {
        return m_qtSettingPanel.getPreferredFocusedComponent();
    }

    @Override
    public @Nullable JComponent createComponent() {
        m_qtSettingPanel = new QtSettingPanel();
        return m_qtSettingPanel.getPanel();
    }

    @Override
    public boolean isModified() {
        QtSettingStateGlobal settings = QtSettingStateGlobal.getInstance();
        return !m_qtSettingPanel.getQtKitPath().equals(settings.getQtCMakeWrapperPath()) ||
                !m_qtSettingPanel.getQtInstallRootPath().equals(settings.getQtInstallRootPath()) ||
                !m_qtSettingPanel.getCmakeFlags().equals(settings.getExtraCmakeFlags()) ||
                !m_qtSettingPanel.getNinjaPath().equals(settings.getNinjaPath());
    }

    @Override
    public void apply() {
        final String qtCmakeLocation = m_qtSettingPanel.getQtKitPath();
        if (!Helpers.validateQtCmakeFile(qtCmakeLocation)) {
            Messages.showErrorDialog("Can't find qt-cmake at the location", "Invalid Location");
            return;
        }

        final String qtRootPath = m_qtSettingPanel.getQtInstallRootPath();
        if (!Helpers.validateQtInstallRootPath(qtRootPath)) {
            Messages.showErrorDialog("Can't find CMake or Ninja at the Qt installation location. " +
                    "Path invalid or lacks appropriate permissions.", "Invalid Location");
            return;
        }

        QtSettingStateGlobal settings = QtSettingStateGlobal.getInstance();
        settings.setQtCMakeWrapperPath(qtCmakeLocation);
        settings.setQtInstallRootPath(qtRootPath);
        settings.setExtraCmakeFlags(m_qtSettingPanel.getCmakeFlags());
        settings.setNinjaPath(m_qtSettingPanel.getNinjaPath());
    }

    @Override
    public void reset() {
        QtSettingStateGlobal settings = QtSettingStateGlobal.getInstance();
        m_qtSettingPanel.setQtKitPath(settings.getQtCMakeWrapperPath());
        m_qtSettingPanel.setQtInstallRootPath(settings.getQtInstallRootPath());
        m_qtSettingPanel.setCmakeFlags(settings.getExtraCmakeFlags());
        m_qtSettingPanel.setNinjaPath(settings.getNinjaPath());
    }

    @Override
    public void disposeUIResources() {
        m_qtSettingPanel = null;
    }
}

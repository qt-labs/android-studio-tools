// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;
import com.intellij.util.xmlb.annotations.OptionTag;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@State(
        name = "org.qtproject.qt.androidstudiotools.QtSettingStateGlobal",
        storages = @Storage("qtPluginSettingsGlobal.xml")
)
public class QtSettingStateGlobal implements PersistentStateComponent<QtSettingStateGlobal> {

    @OptionTag("qtCMakeWrapperPath")
    private String m_qtKitPath = "";
    @OptionTag("qtInstallRootPath")
    private String m_qtInstallRootPath = "";
    @OptionTag("extraCMakeFlags")
    private String m_extraCmakeFlags = "";
    @OptionTag("ninjaPath")
    private String m_ninjaPath = "";

    public static QtSettingStateGlobal getInstance() {
        return ApplicationManager.getApplication().getService(QtSettingStateGlobal.class);
    }

    @Override
    public @Nullable QtSettingStateGlobal getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull QtSettingStateGlobal state) {
        XmlSerializerUtil.copyBean(state, this);
    }

    public String getQtCMakeWrapperPath() {
        return m_qtKitPath;
    }

    public String getQtInstallRootPath() {
        return m_qtInstallRootPath;
    }

    public String getExtraCmakeFlags() {
        return m_extraCmakeFlags;
    }

    public String getNinjaPath() {
        return m_ninjaPath;
    }

    public void setQtCMakeWrapperPath(@NotNull String qtCMakeWrapperPath) {
        m_qtKitPath = qtCMakeWrapperPath;
    }

    public void setQtInstallRootPath(@NotNull String qtInstallRootPath) {
        m_qtInstallRootPath = qtInstallRootPath;
    }

    public void setExtraCmakeFlags(@NotNull String extraCmakeFlags) {
        m_extraCmakeFlags = extraCmakeFlags;
    }

    public void setNinjaPath(@NotNull String ninjaPath) {
        m_ninjaPath = ninjaPath;
    }

}

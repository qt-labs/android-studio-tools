// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import com.intellij.icons.AllIcons;
import com.intellij.ide.projectView.PresentationData;
import com.intellij.ide.projectView.ProjectView;
import com.intellij.ide.util.treeView.AbstractTreeNode;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.SimpleTextAttributes;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

class QtProjectNode extends AbstractTreeNode<VirtualFile> {

    protected QtProjectNode(Project project, @NotNull VirtualFile value) {
        super(project, value);
    }

    @Override
    protected VirtualFile getVirtualFile() {
        return getValue();
    }

    @Override
    public @NotNull Collection<? extends AbstractTreeNode<?>> getChildren() {
        List<VirtualFile> children = new ArrayList<>(Arrays.asList(getValue().getChildren()));
        if (children.isEmpty()) {
            return Collections.emptyList();
        }

        boolean alwaysOnTop = ProjectView.getInstance(myProject).isFoldersAlwaysOnTop("");
        children.sort((o1, o2) -> {
            if (alwaysOnTop) {
                boolean d1 = o1.isDirectory();
                boolean d2 = o2.isDirectory();
                if (d1 && !d2)
                    return -1;
                if (!d1 && d2)
                    return 1;
            }

            return StringUtil.naturalCompare(o1.getName(), o2.getName());
        });

        List<AbstractTreeNode<?>> nodes = new ArrayList<>(children.size());
        for (VirtualFile child : children) {
            if (child.isDirectory() && child.getName().equals("build"))
                continue;

            nodes.add(new QtProjectNode(myProject, child));
        }

        return nodes;
    }

    @Override
    protected void update(@NotNull PresentationData presentation) {
        presentation.setIcon(getValue().isDirectory() ?
                AllIcons.Nodes.Folder :
                getValue().getFileType().getIcon());
        presentation.setPresentableText(getValue().getName());
        if (getValue().isDirectory()) {
            presentation.addText(getValue().getPresentableName(), SimpleTextAttributes.GRAY_ATTRIBUTES);
        }
        presentation.setTooltip(getValue().getPath());
    }

    @Override
    public boolean canNavigate() {
        return !getValue().isDirectory();
    }

    @Override
    public boolean canNavigateToSource() {
        return canNavigate();
    }

    @Override
    public void navigate(boolean requestFocus) {
        FileEditorManager.getInstance(myProject).openFile(getValue(), false);
    }
}

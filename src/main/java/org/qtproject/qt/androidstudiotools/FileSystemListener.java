// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import java.io.File;
import java.util.List;

import org.jetbrains.annotations.NotNull;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.newvfs.BulkFileListener;
import com.intellij.openapi.vfs.newvfs.events.VFileEvent;


final class FileSystemListener implements BulkFileListener {
    private final Project m_project;
    private final QtSettingStateProject m_settings;

    FileSystemListener(Project project) {
        this.m_project = project;
        this.m_settings = QtSettingStateProject.getInstance(project);
    }

    @Override
    public void after(@NotNull List<? extends VFileEvent> events) {
        for (VFileEvent event : events) {
            if (new File(event.getPath()).getAbsolutePath().contains(m_settings.getQtProjectPath()))
                Helpers.refreshProjectView(m_project);
        }
    }
}

// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools

import com.android.tools.idea.gradle.project.sync.GradleSyncListener
import com.android.tools.idea.gradle.project.sync.GradleSyncState
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.ProjectActivity

internal class ProjectStartup : ProjectActivity {

    override suspend fun execute(project: Project) {
        GradleFileParser.parseGradleFile(project)

        GradleSyncState.subscribe(project, object: GradleSyncListener {
            override fun syncSucceeded(project: Project) {
                GradleFileParser.parseGradleFile(project)
            }
        })
    }
}

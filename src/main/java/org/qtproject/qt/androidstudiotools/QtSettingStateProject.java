// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.project.Project;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@State(
        name = "org.qtproject.qt.androidstudiotools.QtSettingStateProject",
        storages = @Storage("qtPluginSettingsProject.xml")
)
public class QtSettingStateProject implements PersistentStateComponent<QtSettingStateProject> {

    private String m_qtProjectPath = "";
    private String m_minimumQtVersion = "";
    private String m_qtPath = "";

    public static QtSettingStateProject getInstance(Project project) {
        return project.getService(QtSettingStateProject.class);
    }

    @Override
    public @Nullable QtSettingStateProject getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull QtSettingStateProject state) {
        XmlSerializerUtil.copyBean(state, this);
    }

    public String getMinimumQtVersion() {
        return m_minimumQtVersion;
    }

    public String getQtProjectPath() {
        return m_qtProjectPath;
    }

    public String getQtPath() {
        return m_qtPath;
    }

    public void setQtMinimumVersion(@NotNull String minimumVersion) {
        m_minimumQtVersion = minimumVersion;
    }

    public void setQtProjectPath(@NotNull String qtProjectPath) {
        m_qtProjectPath = qtProjectPath;
    }

    public void setQtPath(@NotNull String qtPath) {
        m_qtPath = qtPath;
    }

}

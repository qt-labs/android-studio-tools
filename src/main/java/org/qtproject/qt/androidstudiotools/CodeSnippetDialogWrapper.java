// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.util.SystemInfoRt;
import com.intellij.ui.components.JBTextArea;
import org.apache.commons.lang3.SystemUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class CodeSnippetDialogWrapper extends DialogWrapper {
    private final JBTextArea m_kotlinCodeArea = new JBTextArea();
    private final JBTextArea m_groovyCodeArea = new JBTextArea();
    private final JLabel m_infoLabel = new JLabel();
    private final JLabel m_clipboardInfoLabel = new JLabel();
    private final String m_kotlinCode;
    private final String m_groovyCode;
    private final JButton m_codeButton = new JButton();

    protected CodeSnippetDialogWrapper(String newProjectLocation, String qtPathLocation) {
        super(true);
        setTitle("Qt Gradle Plugin Snippet");

        m_kotlinCode = String.format(
                "// plugins {\n" +
                "// Only add the following Qt Gradle Plugin line to your existing plugins {} section\n" +
                "//     id(\"org.qtproject.qt.gradleplugin\") version(\"1.+\")\n" +
                "// }\n\n" +
                "QtBuild {\n" +
                "    qtPath = file(\"%s\")\n" +
                "    projectPath = file(\"%s\")\n" +
                "}\n\n" +
                "// android {\n" +
                "// For Qt 6.8 or older, add the following to your existing android {} section\n" +
                "//    packagingOptions.jniLibs.useLegacyPackaging = true\n" +
                "// }" , qtPathLocation, newProjectLocation);
        m_groovyCode = String.format(
                "// plugins {\n" +
                "// Only add the following Qt Gradle Plugin line to your existing plugins {} section\n" +
                "//    id 'org.qtproject.qt.gradleplugin' version '1.+'\n" +
                "// }\n\n" +
                "QtBuild {\n" +
                "    qtPath file('%s')\n" +
                "    projectPath file('%s')\n" +
                "}\n\n" +
                "// android {\n" +
                "// For Qt 6.8 or older, add the following to your existing android {} section\n" +
                "//    packagingOptions.jniLibs.useLegacyPackaging = true\n" +
                "// }" , qtPathLocation, newProjectLocation);

        init();
    }

    private void setupCodeCopyButton(JButton button, String code, String codeLanguage) {
        String copyText = codeLanguage.equals("Kotlin") ? "Copy Kotlin snippet" : "Copy Groovy snippet";
        button.setText(copyText);

        for (ActionListener listener : button.getActionListeners()) {
            button.removeActionListener(listener);
        }
        button.addActionListener(e -> {
            StringSelection selection = new StringSelection(code);
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
            String message = String.format("Qt project %s snippet has been copied to the clipboard.", codeLanguage);
            m_clipboardInfoLabel.setText(message);
        });
    }

    @Override
    protected @NotNull JPanel createButtonsPanel(@NotNull List<? extends JButton> buttons) {
        setupCodeCopyButton(m_codeButton, m_kotlinCode, "Kotlin");
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(m_codeButton);
        buttonPanel.add(buttons.get(SystemInfoRt.isMac ? 1 : 0));
        return buttonPanel;
    }


    @Override
    protected @Nullable JComponent createCenterPanel() {
        JTextArea[] codeAreas = {m_kotlinCodeArea, m_groovyCodeArea};
        int padding = 5;

        for (JTextArea codeArea : codeAreas) {
            codeArea.setLineWrap(false);
            codeArea.setWrapStyleWord(false);
            codeArea.setEditable(false);
            codeArea.setBorder(new EmptyBorder(padding, padding, padding, padding));
        }
        m_kotlinCodeArea.setText(m_kotlinCode);
        m_groovyCodeArea.setText(m_groovyCode);

        // TODO: Add link to the QtGP release docs
        String QtGPDocumentPage = "https://doc.qt.io/qtgradleplugin/";
        m_infoLabel.setText(String.format("<html>Qt for Android Studio Tools is now using the " +
                "<a href=\"%s\">Qt Gradle plugin</a>.<br>" +
                "Copy the provided Kotlin/Groovy snippet to your app/build.gradle file, " +
                "to add the Qt project to your current project.<br>",QtGPDocumentPage));
        m_infoLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
        m_infoLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (GraphicsEnvironment.isHeadless())
                    return;

                try {
                    // Open the link in the default browser
                    if (SystemUtils.IS_OS_LINUX) {
                        Runtime currentRuntime = Runtime.getRuntime();

                        if (currentRuntime.exec(new String[] { "which", "xdg-open" })
                                .getInputStream().read() != -1) {
                            currentRuntime.exec(new String[] { "xdg-open", QtGPDocumentPage });
                        }
                    } else if (Desktop.isDesktopSupported()) {
                        Desktop.getDesktop().browse(new URI(QtGPDocumentPage));
                    }
                } catch (IOException | SecurityException | URISyntaxException ex) {
                    ex.printStackTrace();
                }
            }
        });

        JTabbedPane tabbedPane = new JTabbedPane();
        JPanel kotlinPanel = new JPanel();
        kotlinPanel.setLayout(new BorderLayout());
        kotlinPanel.add(m_kotlinCodeArea, BorderLayout.CENTER);

        JPanel groovyPanel = new JPanel();
        groovyPanel.setLayout(new BorderLayout());
        groovyPanel.add(m_groovyCodeArea, BorderLayout.CENTER);

        tabbedPane.addTab("Kotlin", kotlinPanel);
        tabbedPane.addTab("Groovy", groovyPanel);

        tabbedPane.addChangeListener(e-> {
            if (tabbedPane.getSelectedIndex() == 0) {
                setupCodeCopyButton(m_codeButton, m_kotlinCode, "Kotlin");
            } else {
                setupCodeCopyButton(m_codeButton, m_groovyCode, "Groovy");
            }
        });

        JPanel additionalPanel = new JPanel();
        additionalPanel.setLayout(new BorderLayout());
        additionalPanel.add(m_infoLabel, BorderLayout.NORTH);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(additionalPanel, BorderLayout.NORTH);
        mainPanel.add(tabbedPane, BorderLayout.CENTER);
        mainPanel.add(m_clipboardInfoLabel, BorderLayout.SOUTH);

        return mainPanel;
    }
}

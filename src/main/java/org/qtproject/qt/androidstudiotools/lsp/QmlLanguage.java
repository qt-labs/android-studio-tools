// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools.lsp;

import com.intellij.lang.Language;

public class QmlLanguage extends Language {
    private static final QmlLanguage INSTANCE = new QmlLanguage();

    public static QmlLanguage getInstance() {
        return INSTANCE;
    }

    protected QmlLanguage() {
        super("QML");
    }
}

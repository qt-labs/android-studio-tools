// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0


package org.qtproject.qt.androidstudiotools.lsp;

import com.intellij.openapi.util.IconLoader;

import javax.swing.Icon;

public class QmlIcons {
    private static final Icon FILE = IconLoader.getIcon(
            "/img/fileoverlay_qml.png",
            QmlIcons.class
    );

    public static Icon getFile() {
        return FILE;
    }
}

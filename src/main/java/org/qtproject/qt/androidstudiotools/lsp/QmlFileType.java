// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools.lsp;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.util.NlsContexts;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

import javax.swing.Icon;

public class QmlFileType extends LanguageFileType {
    private static final QmlFileType INSTANCE = new QmlFileType();

    public static QmlFileType getInstance() {
        return INSTANCE;
    }

    protected QmlFileType() {
        super(QmlLanguage.getInstance());
    }

    @Override
    public @NonNls @NotNull String getName() {
        return "QML file";
    }

    @Override
    public @NlsContexts.Label @NotNull String getDescription() {
        return "Qt QML file type";
    }

    @Override
    public @NotNull String getDefaultExtension() {
        return "qml";
    }

    @Override
    public Icon getIcon() {
        return QmlIcons.getFile();
    }
}

// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools.lsp;

import com.intellij.execution.configurations.GeneralCommandLine;
import com.intellij.openapi.project.Project;
import com.intellij.util.text.VersionComparatorUtil;
import com.redhat.devtools.lsp4ij.server.OSProcessStreamConnectionProvider;
import org.qtproject.qt.androidstudiotools.Helpers;
import org.qtproject.qt.androidstudiotools.QtSettingStateProject;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class QmlLanguageServer extends OSProcessStreamConnectionProvider {

    public QmlLanguageServer(Project project) {
        QtSettingStateProject projectSettings = QtSettingStateProject.getInstance(project);

        String qtPath = projectSettings.getQtPath();
        String qtProjectPath = projectSettings.getQtProjectPath();

        String qtProjectName = Paths.get(qtProjectPath).getFileName().toString();

        GeneralCommandLine commandLine = new GeneralCommandLine(
                getQmlLsPath(qtPath),
                "--build-dir", getProjectBuildPath(project.getBasePath(), qtProjectName)
        );

        String currentQtVersion = Paths.get(qtPath).getFileName().toString();

        if (VersionComparatorUtil.compare(currentQtVersion, "6.8.1") >= 0)
            commandLine.addParameters("-d", getQmlDocsPath(qtPath, currentQtVersion));

        super.setCommandLine(commandLine);
    }

    private String getQmlLsPath(String qtPath) {
        File[] abiFolders = new File(qtPath).listFiles();
        if (abiFolders == null)
            return "";

        for (File file : abiFolders) {
            if (file.isDirectory()) {
                Path qmlLs = Paths.get(file.getAbsolutePath(), "bin",
                        Helpers.formatBinaryName("qmlls"));
                if (qmlLs.toFile().exists()) {
                    return qmlLs.toString();
                }
            }
        }

        return "";
    }

    private String getProjectBuildPath(String projectPath, String qtProjectName) {
        Path buildPath = Paths.get(projectPath, "app", "build", "qt_generated", qtProjectName);
        return buildPath.toString();
    }

    private String getQmlDocsPath(String qtPath, String qtVersion) {
        String qtRootDirectory = Paths.get(qtPath).getParent().toString();
        return Paths.get(qtRootDirectory, "Docs", "Qt-" + qtVersion).toString();
    }
}

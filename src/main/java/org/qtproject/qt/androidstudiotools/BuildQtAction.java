// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import com.intellij.execution.executors.DefaultRunExecutor;
import com.intellij.openapi.actionSystem.ActionUpdateThread;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.externalSystem.model.execution.ExternalSystemTaskExecutionSettings;
import com.intellij.openapi.externalSystem.service.execution.ProgressExecutionMode;
import com.intellij.openapi.externalSystem.util.ExternalSystemUtil;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.plugins.gradle.util.GradleConstants;

import java.util.Collections;

class BuildQtAction extends AnAction {

    public static final String QT_BUILD_TASK_NAME = "QtBuildTask";

    // If targeting 2022.3 or later override getActionUpdateThread
    @Override
    public @NotNull ActionUpdateThread getActionUpdateThread() {
        return ActionUpdateThread.BGT;
    }

    @Override
    public void update(@NotNull AnActionEvent e) {
        super.update(e);

        if (e.getProject() == null)
            return;

        QtSettingStateProject settingsProject = QtSettingStateProject.getInstance(e.getProject());
        e.getPresentation().setEnabled(!settingsProject.getQtProjectPath().isBlank());
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        ProgressManager.getInstance().run(new Task.Backgroundable(e.getProject(), "Building Qt project") {
            @Override
            public void run(@NotNull ProgressIndicator progressIndicator) {
                if (e.getProject() == null)
                    return;

                ExternalSystemTaskExecutionSettings gradleSettings = new ExternalSystemTaskExecutionSettings();
                gradleSettings.setExternalProjectPath(e.getProject().getBasePath());
                gradleSettings.setTaskNames(Collections.singletonList(QT_BUILD_TASK_NAME));
                gradleSettings.setVmOptions("");
                gradleSettings.setExternalSystemIdString(GradleConstants.SYSTEM_ID.getId());

                ExternalSystemUtil.runTask(
                        gradleSettings,
                        DefaultRunExecutor.EXECUTOR_ID,
                        e.getProject(),
                        GradleConstants.SYSTEM_ID,
                        null,
                        ProgressExecutionMode.IN_BACKGROUND_ASYNC,
                        true);
            }
        });
    }
}

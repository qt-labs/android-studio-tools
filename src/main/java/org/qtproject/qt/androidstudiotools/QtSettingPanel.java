// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.androidstudiotools;

import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.ui.TextBrowseFolderListener;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.ui.RawCommandLineEditor;
import com.intellij.util.ui.FormBuilder;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.apache.commons.lang3.SystemUtils;

class QtSettingPanel {

    private final JPanel m_mainPanel;
    private final TextFieldWithBrowseButton m_qtKitPathField = new TextFieldWithBrowseButton();
    private final TextFieldWithBrowseButton m_qtInstallRootPathField = new TextFieldWithBrowseButton();
    private final TextFieldWithBrowseButton m_ninjaPathField = new TextFieldWithBrowseButton();
    private final RawCommandLineEditor m_extraCMakeFlagsField = new RawCommandLineEditor();

    public QtSettingPanel() {
        String qtKitText = "";
        String rootPathText = "";
        String minimumQtVersion = NewProjectDialogWrapper.getMinimumQtVersionExample() + ".0";
        if (SystemUtils.IS_OS_WINDOWS) {
                rootPathText = "e.g. C:\\Qt";
                qtKitText = "e.g. C:\\Qt\\" + minimumQtVersion + "\\android_arm64_v8a";
        } else if (SystemUtils.IS_OS_MAC_OSX) {
                rootPathText = "e.g. /Users/johndoe/Qt";
                qtKitText = "e.g. /Users/johndoe/Qt/" + minimumQtVersion + "/android_arm64_v8a";
        } else {
                rootPathText = "e.g. /home/johndoe/Qt";
                qtKitText = "e.g. /home/johndoe/Qt/" + minimumQtVersion + "/android_arm64_v8a";
        }
        m_qtKitPathField.getTextField().setToolTipText(qtKitText);
        m_qtKitPathField.addBrowseFolderListener(new TextBrowseFolderListener(new FileChooserDescriptor(
                true,
                false,
                false,
                false,
                false,
                false
        )));

        m_qtInstallRootPathField.getTextField().setToolTipText(rootPathText);
        m_qtInstallRootPathField.addBrowseFolderListener(new TextBrowseFolderListener(new FileChooserDescriptor(
                false,
                true,
                false,
                false,
                false,
                false
        )));

        m_ninjaPathField.addBrowseFolderListener(new TextBrowseFolderListener(new FileChooserDescriptor(
                false,
                true,
                false,
                false,
                false,
                false
        )));

        m_extraCMakeFlagsField.getTextField().setToolTipText(
                "e.g. -DCMAKE_BUILD_TYPE=Release -DQT_ANDROID_ABIS:STRING=x86_64");

        m_mainPanel = FormBuilder.createFormBuilder()
                .addLabeledComponent(
                        new JLabel("Location of Qt Kit: "),
                        m_qtKitPathField,
                        1,
                        false
                )
                .addLabeledComponent(
                        new JLabel("Location of Qt Installation: "),
                        m_qtInstallRootPathField,
                        1,
                        false
                ).addLabeledComponent(
                        new JLabel("Custom Ninja Path: "),
                        m_ninjaPathField,
                        1,
                        false
                ).addLabeledComponent(
                        new JLabel("Additional CMake flags: "),
                        m_extraCMakeFlagsField,
                        1,
                        false
                ).getPanel();
    }

    public JPanel getPanel() {
        return m_mainPanel;
    }

    public JComponent getPreferredFocusedComponent() {
        return m_qtKitPathField;
    }

    public String getQtKitPath() {
        return m_qtKitPathField.getText();
    }

    public String getQtInstallRootPath() {
        return m_qtInstallRootPathField.getText();
    }

    public void setQtKitPath(String newLocation) {
        m_qtKitPathField.setText(newLocation);
    }

    public String getCmakeFlags() { return m_extraCMakeFlagsField.getText(); }

    public void setQtInstallRootPath(String newLocation) {
        m_qtInstallRootPathField.setText(newLocation);
    }

    public void setCmakeFlags(String newFlags) { m_extraCMakeFlagsField.setText(newFlags); }

    public String getNinjaPath() { return m_ninjaPathField.getText(); }

    public void setNinjaPath(String ninjaPath) { m_ninjaPathField.setText(ninjaPath); }
}

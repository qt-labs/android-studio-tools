# Qt plugin for Android Studio

This plugin supports the integration of Qt/QML projects into Android Studio projects.

### Functionality of the plugin

- You can import a Qt project from File > New > Import Qt Project...
- You'll need to give the path to a local qt-cmake file in Settings > Build, Execution, Deployment >
    Qt. For example a path might look like this: /home/user/Qt/6.5.0/android_x86_64/bin/qt-cmake.
- After the qt-cmake path is configured and the Qt project is imported, you can build the Qt project
    using Build > Build Qt Project.


### Dependencies

The plugin requires IntelliJ 2024.1.2 or newer i.e. Android Studio Koala Feature Drop or later.

### How to build

Create a `local.properties` file in the project root and set the following property to the path
of your local Android Studio path, for example for Linux:

```properties
android-studio.dir=~/android-studio/
```

On macOS

```properties
android-studio.dir=/Applications/Android\ Studio.app/Contents
```

To build the plugin, run the following:

```bash
./gradlew buildPlugin
```

If you don't have the JDK path set via JAVA_HOME or via Gradle parameter `-Dorg.gradle.java.home`.

```bash
./gradlew buildPlugin -Dorg.gradle.java.home=<path_to_jdk>
```

To do the plugin build and run Android Studio with the plugin in a sandbox environment,
run the following:

```bash
./gradlew runIde
```

If you run into the following error:

> Unsupported JVM architecture was selected for running Gradle tasks: x86_64.
  Supported architectures: aarch64

You might need to set a different JDK that supports arm64, or use the JDK shipped with
Android Studio.
